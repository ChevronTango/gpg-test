# GPG Test

Fulcio Config

```json
{
  "OIDCIssuers": {
    "https://api.gitpod.io/idp": {
      "IssuerURL": "https://api.gitpod.io/idp",
      "ClientID": "sigstore",
      "Type": "email"
    }
  }
}
```

Setup Gitsign


```console
brew tap sigstore/tap
brew install gitsign
brew install cosign
```

```
git config --global commit.gpgsign true  # Sign all commits
git config --global tag.gpgsign true  # Sign all tags
git config --global gpg.x509.program gitsign
git config --global gpg.format x509  # Gitsign expects x509 args

git add .
git commit -m "My Changes"
git push
```

```
docker login registry.gitlab.com -u chevrontango
docker build -t registry.gitlab.com/chevrontango/gpg-test .
docker push registry.gitlab.com/chevrontango/gpg-test
cosign sign registry.gitlab.com/chevrontango/gpg-test
```


```console
mkdir ~/bin
cp ./gpgitsign ~/bin/gpgitsign
chmod +x ~/bin/gpgitsign
echo 'export PATH=$PATH:~/bin' >> ~/.bashrc

git config --global commit.gpgsign true  # Sign all commits
git config --global tag.gpgsign true  # Sign all tags
git config --global gpg.x509.program gpgitsign
git config --global gpg.format x509  # Gitsign expects x509 args
git config --global gitsign.fulcio https://5555-chevrontango-gpgtest-64pn8oa7a3u.ws-eu96b.gitpod.io
```

```
export SIGSTORE_ID_TOKEN=$(gp idp token --audience sigstore)
```